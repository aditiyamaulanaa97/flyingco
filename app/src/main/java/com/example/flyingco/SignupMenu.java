package com.example.flyingco;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupMenu extends AppCompatActivity {

    EditText inputEmail, inputPass;
    String email, password;
    Button btnDaftar;
    FirebaseAuth fAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup_menu);
        fAuth = FirebaseAuth.getInstance();
        inputEmail = findViewById(R.id.txtemail);
        inputPass = findViewById(R.id.txtpass);
        btnDaftar = findViewById(R.id.btnRegister);

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inputEmail.getText().toString().equals("")) {
                    inputEmail.setError("Please, Fill Your Email");
                } else if (inputPass.getText().toString().equals("")) {
                    inputPass.setError("Please, Fill Your Password");
                } else {

                    registration();
                }
            }
        });
        }

        private void registration(){
            email = inputEmail.getText().toString();
            password = inputPass.getText().toString();

            fAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(SignupMenu.this, "Sign Up Success!", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SignupMenu.this, LoginMenu.class));
                                finish();
                            }else {
                                Toast.makeText(SignupMenu.this, "Sign Up Not Success!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    public void onBack(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                Intent intent = new Intent(this, LoginMenu.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
package com.example.flyingco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Website extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_website);

        WebView webView = (WebView) findViewById(R.id.webPesawat);
//        arahkan ke url website
        webView.loadUrl("https://www.garuda-indonesia.com/id/id/index");
//        jalankan fungsi webclient
        webView.setWebViewClient(new WebViewClient());
//        aktifkan JS
        webView.getSettings().setJavaScriptEnabled(true);
    }
    public void onExit(View v) {
        switch (v.getId()) {
            case R.id.btnKembali:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
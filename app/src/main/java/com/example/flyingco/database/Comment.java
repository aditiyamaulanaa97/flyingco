package com.example.flyingco.database;

import android.provider.BaseColumns;

public class Comment {
    private Comment(){

    }
    public static final class CommantDetails implements BaseColumns{
        public static final String TABLE_NAME = "comments";
        public static final String COL_ID = "id";
        public static final String COL_COMMENTS = "comments";
    }
}

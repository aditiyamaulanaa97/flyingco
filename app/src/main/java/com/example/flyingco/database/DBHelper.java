package com.example.flyingco.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "Comments.db";
    public static final int DB_VERSION = 1;
    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String COMMENT_TABLE = "CREATE TABLE "+ Comment.CommantDetails.TABLE_NAME+" ( "+
                Comment.CommantDetails.COL_ID+ "INTEGER PRIMARY KEY, "+
                Comment.CommantDetails.COL_COMMENTS+" TEXT )";
        db.execSQL(COMMENT_TABLE);

    }

    //Insert Comment
    public boolean addComments(int id, String comments){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Comment.CommantDetails.COL_ID,id);
        values.put(Comment.CommantDetails.COL_COMMENTS,comments);

        long sid = db.insert(Comment.CommantDetails.TABLE_NAME, null, values);
        if (sid>0){
            return true;
        }else
            return false;

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

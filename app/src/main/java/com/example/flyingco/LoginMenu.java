package com.example.flyingco;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginMenu extends AppCompatActivity {

    EditText inputEmail, inputPass;
    String email, password;
    Button btn_Login;
    private FirebaseAuth fAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu_login);
        fAuth = FirebaseAuth.getInstance();
        inputEmail= findViewById(R.id.txtUsername);
        inputPass = findViewById(R.id.txtPassword);
        btn_Login = findViewById(R.id.btnLogin);
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inputEmail.getText().toString().equals("")) {
                    inputEmail.setError("Please, Fill Your Email");
                } else if(inputPass.getText().toString().equals("")) {
                    inputPass.setError("Please, Fill Your Password");
                } else {
                    login();
                }
            }
        });
    }

    private void login(){
        email = inputEmail.getText().toString();
        password = inputPass.getText().toString();
        fAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this,
                        new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(LoginMenu.this, "Login Success!", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LoginMenu.this, MainActivity.class));
                        }else {
                            Toast.makeText(LoginMenu.this, "Email/Password Wrong!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                );
    }

    public void signUp(View view){
        Intent intent = new Intent(LoginMenu.this, SignupMenu.class);
        startActivity(intent);
    }
}

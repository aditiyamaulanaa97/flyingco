package com.example.flyingco.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.flyingco.R;
import com.example.flyingco.model.ModelReservation;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.MyViewHolder> {

    private ArrayList<ModelReservation> mList;
    private Activity activity;
//    private LayoutInflater inflater;
    private Context context;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    public ReservationAdapter(Context context, ArrayList<ModelReservation> mList){
        this.context = context;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ReservationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = inflater.inflate(R.layout.layout_reservation_ticket, parent, false);
        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationAdapter.MyViewHolder holder, int position) {
        final ModelReservation data = mList.get(position);
        holder.tvNama.setText(data.getNamaPenumpang());
        holder.tvHargaTiket.setText(data.getHargaTiket());
        holder.tvKelas.setText(data.getKelas());
        holder.tvDate.setText(data.getDate());
        holder.tvKeberangkatan.setText(data.getKeberangkatan());
        holder.tvTujuan.setText(data.getTujuan());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate, tvKelas, tvNama, tvHargaTiket,tvKeberangkatan, tvTujuan;
        ImageView imageProfile;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvKelas = itemView.findViewById(R.id.tvKelas);
            tvHargaTiket = itemView.findViewById(R.id.tvHargaTiket);
            tvKeberangkatan = itemView.findViewById(R.id.tvTujuan);
            tvTujuan = itemView.findViewById(R.id.tvTujuan);
            imageProfile = itemView.findViewById(R.id.imageProfile);
        }
    }
}

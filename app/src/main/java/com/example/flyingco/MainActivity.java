package com.example.flyingco;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.flyingco.adapter.RecentsAdapter;
import com.example.flyingco.fragment.FirstFragment;
import com.example.flyingco.fragment.SecondFragment;
import com.example.flyingco.fragment.ThirdFragment;
import com.example.flyingco.model.RecentsData;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recentRecycler;
    RecentsAdapter recentsAdapter;
    Button reservation, history, profile;
    DrawerLayout mDrawer;
    Toolbar toolbar;
    NavigationView nvDrawer;
    ActionBarDrawerToggle drawerToggle;
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        frameLayout=findViewById(R.id.flContent);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //DrawerView
        mDrawer = findViewById(R.id.myDrawerLayout);

        //Drawer Navigation
        nvDrawer = findViewById(R.id.nav_view);
        setupDrawerContent(nvDrawer);

        //List Content Home
        List<RecentsData> recentsDataList = new ArrayList<>();
        recentsDataList.add(new RecentsData("Pura Ulun Danu","Indonesia","From $350",R.drawable.bali));
        recentsDataList.add(new RecentsData("Borobudur","Indonesia","From $300",R.drawable.borobudur));
        recentsDataList.add(new RecentsData("Labuan Bajo","Indonesia","From $400",R.drawable.labuanbajo));
        recentsDataList.add(new RecentsData("Kawah Putuh","Indonesia","From $300",R.drawable.kawahputih));
        recentsDataList.add(new RecentsData("AM Lake","India","From $200",R.drawable.recentimage1));
        recentsDataList.add(new RecentsData("Nilgiri Hills","India","From $300",R.drawable.recentimage2));

        setRecentRecycler(recentsDataList);
    }

    public void buttonMain (View v){
        switch (v.getId()) {
            case R.id.navHome:
                Intent intentLogin = new Intent(this, MainActivity.class);
                startActivity(intentLogin);
                finish();
                break;
        }
        switch (v.getId()) {
            case R.id.navLike:
                Intent intenComment = new Intent(this, CommentActivity.class);
                startActivity(intenComment);
                finish();
                break;
        }
        switch (v.getId()) {
            case R.id.navReservation:
                Intent intenReservation = new Intent(this, ReservationTicket.class);
                startActivity(intenReservation);
                finish();
                break;
        }
        switch (v.getId()) {
            case R.id.navHistory:
                Intent intentHistory = new Intent(this, HistoryReservation.class);
                startActivity(intentHistory);
                finish();
                break;
        }
    }

    private  void setRecentRecycler(List<RecentsData> recentsDataList){
        recentRecycler = findViewById(R.id.rowItem);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recentRecycler.setLayoutManager(layoutManager);
        recentsAdapter = new RecentsAdapter(this, recentsDataList);
        recentRecycler.setAdapter(recentsAdapter);

    }

    private void setupDrawerContent(NavigationView nvDrawer) {
        nvDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });
    }

    private void selectDrawerItem(MenuItem item) {
        Fragment fragment = null;
        Class fragmentClass;

        switch(item.getItemId()){
            case R.id.nav_first_fragment:
                fragmentClass = SecondFragment.class;
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_second_fragment:
                fragmentClass = SecondFragment.class;
                Intent intent1 = new Intent(this, Website.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.nav_third_fragment:
                fragmentClass = ThirdFragment.class;
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Apakah anda ingin keluar?")
                        .setPositiveButton("Ya", ((dialogInterface, i) -> {
                            Intent intenthome = new Intent(MainActivity.this, LoginMenu.class);
                            startActivity(intenthome);
                            Toast.makeText(MainActivity.this, "Berhasil Exit!", Toast.LENGTH_SHORT).show();
                            finish();
                        }))
                        .setNegativeButton("Tidak", null)
                        .create();
                dialog.show();
                break;
            default:
                fragmentClass = FirstFragment.class;
        }


        try{
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        item.setChecked(true);
        setTitle(item.getTitle());
        mDrawer.closeDrawers();

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
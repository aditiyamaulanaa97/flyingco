package com.example.flyingco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.flyingco.database.DBHelper;

public class CommentActivity extends AppCompatActivity {

    EditText etId, etComments;
    DBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        etId = findViewById(R.id.txtId);
        etComments = findViewById(R.id.txtComments);

        dbHelper = new DBHelper(this);
    }

    public void btnSubmit(View v) {
        int id = Integer.parseInt(etId.getText().toString());
        String comments = etComments.getText().toString();

        boolean status = dbHelper.addComments(id, comments);

        if (status) {
            Toast.makeText(this, "Inserted Success", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Inserted Fail", Toast.LENGTH_SHORT).show();
        }
    }

    public void onBack(View v) {
        switch (v.getId()) {
            case R.id.btnBackC:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
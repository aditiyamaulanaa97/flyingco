package com.example.flyingco.model;

public class ModelReservation {
    String namaPenumpang;
    String kelas;
    String date;
    String hargaTiket;
    String keberangkatan;
    String tujuan;

//
//
//    public ModelReservation(String namaPenumpang, String kelas, String date, Integer hargaTiket, String keberangkatan, String tujuan) {
//        this.namaPenumpang = namaPenumpang;
//        this.kelas = kelas;
//        this.date = date;
//        this.hargaTiket = hargaTiket;
//        this.keberangkatan = keberangkatan;
//        this.tujuan = tujuan;
//    }


    public String getNamaPenumpang() {
        return namaPenumpang;
    }

    public void setNamaPenumpang(String namaPenumpang) {
        this.namaPenumpang = namaPenumpang;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHargaTiket() {
        return hargaTiket;
    }

    public void setHargaTiket(String hargaTiket) {
        this.hargaTiket = hargaTiket;
    }

    public String getKeberangkatan() {
        return keberangkatan;
    }

    public void setKeberangkatan(String keberangkatan) {
        this.keberangkatan = keberangkatan;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }
}




package com.example.flyingco;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.flyingco.model.ModelReservation;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.HashMap;

public class ReservationTicket extends AppCompatActivity {

    DatabaseReference dbReservation = FirebaseDatabase.getInstance().getReference("History").push();

    MaterialButton btPesan;
    EditText etDate;
    String tujuan, keberangkatan, namaPenumpan, noTelp, kelas, status;
    Integer hargaTiket, total;
    int jumlah, hargaTotal;
    String ItemKeberangkatan, ItemTujuan, ItemKelas;
    Button btn;
    ImageView btnadd, btnminus;
    Integer countPenumpang = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_reservation_ticket);

        final String[] asal = {"Jakarta", "Bandung", "Yogyakarta"};
        final String[] tujuans = {"Jakarta", "Bandung", "Yogyakarta"};

        //GET XML
        EditText nama = (EditText) findViewById(R.id.inputNama);
        EditText noTelpon = (EditText) findViewById(R.id.inputNotelp);
        Spinner brangkat = (Spinner) findViewById((R.id.spBerangkat));
        Spinner tujuan = (Spinner) findViewById((R.id.spTujuan));
        TextView jumlah = (TextView) findViewById(R.id.jmlhPenumpang);
        Spinner kelas = (Spinner) findViewById(R.id.spKelas);
        btnadd = (ImageView) findViewById(R.id.imageAdd2);
        btnminus = (ImageView) findViewById(R.id.imageMinus2);
        btPesan = findViewById(R.id.btnCheckout);

        etDate = (EditText) findViewById(R.id.inputTanggal);

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mYear, mMonth, mDay;
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR); //tahun
                mMonth = c.get(Calendar.MONTH); //bulan
                mDay = c.get(Calendar.DAY_OF_MONTH); //hari berdasarkan bulan
                DatePickerDialog datePickerDialog = new DatePickerDialog(ReservationTicket.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                                etDate.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });

        //GET SPINNER
        Spinner spnBerangkat = (Spinner) findViewById(R.id.spBerangkat);
        Spinner spnTujuan = (Spinner) findViewById(R.id.spTujuan);
        Spinner spnKelas = (Spinner) findViewById(R.id.spKelas);

        spnBerangkat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ItemKeberangkatan = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getBaseContext(), ItemKeberangkatan, Toast.LENGTH_LONG);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getBaseContext(), "", Toast.LENGTH_SHORT).show();
            }
        });

        spnTujuan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ItemTujuan = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getBaseContext(), ItemTujuan, Toast.LENGTH_LONG);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getBaseContext(), "", Toast.LENGTH_SHORT).show();
            }
        });

        spnKelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ItemKelas = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getBaseContext(), ItemKelas, Toast.LENGTH_LONG);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getBaseContext(), "", Toast.LENGTH_SHORT).show();
            }
        });

        btn = findViewById(R.id.btnCheckout);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                perhitunganHarga();
                String getNama = nama.getText().toString();
                String getKeberangkatan = ItemKeberangkatan;
                String getTujuan = ItemTujuan;
                String getJumlah = String.valueOf(jumlah);
                String getKelas = ItemKelas;
                String getDate = etDate.getText().toString();
                String getNoHP = String.valueOf(noTelpon);

                System.out.println(jumlah);
                System.out.println(hargaTotal);
                //Validation Condition
                //Validation Condition 2
                if (getKeberangkatan != null && getTujuan != null && getKeberangkatan != null && getTujuan != null) {
                    if ((getKeberangkatan.equalsIgnoreCase("jakarta") && getTujuan.equalsIgnoreCase("jakarta") && getJumlah.equalsIgnoreCase("0"))
                            || (getKeberangkatan.equalsIgnoreCase("bandung") && getTujuan.equalsIgnoreCase("bandung") && getJumlah.equalsIgnoreCase("0"))
                            || (getKeberangkatan.equalsIgnoreCase("yogyakarta") && getTujuan.equalsIgnoreCase("yogyakarta") && getJumlah.equalsIgnoreCase("0"))) {
                        Toast.makeText(ReservationTicket.this, "Asal dan Tujuan tidak boleh sama !", Toast.LENGTH_LONG).show();
                    }  else if (getNama.isEmpty()) {
                        nama.setError("Please, Fill Your Name");
                    } else if (getJumlah.isEmpty()) {
                        jumlah.setError("Please, Fill");
                    } else if (getNoHP.isEmpty()) {
                        noTelpon.setError("Please, Fill Your Number Phone");
                    }

                } else {
                    AlertDialog dialog = new AlertDialog.Builder(ReservationTicket.this)
                            .setTitle("Ingin booking tiket sekarang?")
                            .setPositiveButton("Ya", ((dialogInterface, i) -> {
                                Intent intenthome = new Intent(ReservationTicket.this, HistoryReservation.class);
                                startActivity(intenthome);
                                Toast.makeText(ReservationTicket.this, "Berhasil Terbooking!", Toast.LENGTH_SHORT).show();
                                finish();
                            }))
                            .setNegativeButton("Tidak", null)
                            .create();
                    dialog.show();
                    Toast.makeText(ReservationTicket.this, "Mohon lengkapi data pemesanan!", Toast.LENGTH_LONG).show();
                }
            }
        });

        //add dewasa
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countPenumpang = countPenumpang + 1;
                jumlah.setText(countPenumpang.toString());
            }
        });

        //min dewasa
        btnminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countPenumpang > 0) {
                    countPenumpang = countPenumpang - 1;
                    jumlah.setText(countPenumpang.toString());
                }
            }
        }
        );

        btPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                perhitunganHarga();
                String getNama = nama.getText().toString();
                String getKeberangkatan = ItemKeberangkatan;
                String getTujuan = ItemTujuan;
                String getJumlah = String.valueOf(jumlah);
                String getKelas = ItemKelas;
                String getDate = etDate.getText().toString();
                String getNoHP = String.valueOf(noTelpon);

                System.out.println(jumlah);
                System.out.println(hargaTotal);
                //Validation Condition
                if (getNama.isEmpty()) {
                    nama.setError("Please, Fill Your Name");
                } else if (getJumlah.isEmpty()) {
                    jumlah.setError("Please, Fill");
                } else if (getNoHP.isEmpty()) {
                    noTelpon.setError("Please, Fill Your Number Phone");
                }

                //Validation Condition 2
                if (getKeberangkatan != null && getTujuan != null && getKeberangkatan != null && getTujuan != null) {
                    if ((getKeberangkatan.equalsIgnoreCase("jakarta") && getTujuan.equalsIgnoreCase("jakarta") && getJumlah.equalsIgnoreCase("0"))
                            || (getKeberangkatan.equalsIgnoreCase("bandung") && getTujuan.equalsIgnoreCase("bandung") && getJumlah.equalsIgnoreCase("0"))
                            || (getKeberangkatan.equalsIgnoreCase("yogyakarta") && getTujuan.equalsIgnoreCase("yogyakarta") && getJumlah.equalsIgnoreCase("0"))) {
                        Toast.makeText(ReservationTicket.this, "Asal dan Tujuan tidak boleh sama !", Toast.LENGTH_LONG).show();
                    } else {
                        AlertDialog dialog = new AlertDialog.Builder(ReservationTicket.this)
                                .setTitle("Ingin booking pesawat sekarang?")
                                .setPositiveButton("Ya", ((dialogInterface, i) -> {
                                    KirimData(getNama,getKelas,getDate,hargaTiket,getKeberangkatan,getTujuan);
                                    Intent intenthome = new Intent(ReservationTicket.this, HistoryReservation.class);
                                    startActivity(intenthome);
                                    Toast.makeText(ReservationTicket.this, "Berhasil Terbooking!", Toast.LENGTH_SHORT).show();
                                    finish();
                                }))
                                .setNegativeButton("Tidak", null)
                                .create();
                        dialog.show();
                    }
                } else {
                    Toast.makeText(ReservationTicket.this, "Mohon lengkapi data pemesanan!", Toast.LENGTH_LONG).show();
                }

//                Intent intent = new Intent(ReservationTicket.this, HistoryReservation.class);
//                intent.putExtra("fullname", nama.getText().toString());
//                intent.putExtra("kelas", ItemKelas);
//                intent.putExtra("tanggal", etDate.getText().toString());
//                intent.putExtra("keberangkatan", ItemKeberangkatan);
//                intent.putExtra("tujuan", ItemTujuan);
//                startActivity(intent);
            }
        });
    }


    public void onBack(View v) {
        switch (v.getId()) {
            case R.id.toolbar:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }


    public void perhitunganHarga() {


        System.out.println(ItemKeberangkatan);
        System.out.println(ItemKelas.toString());
        //SET BERANGKAT-TUJUAN
        if (ItemKeberangkatan.equalsIgnoreCase("jakarta") && ItemTujuan.equalsIgnoreCase("bandung") || ItemKeberangkatan.equalsIgnoreCase("bandung") && ItemTujuan.equalsIgnoreCase("jakarta")) {
            switch (ItemKelas) {
                case "VVIP":
                    hargaTiket = 1700000;
                    break;
                case "Business":
                    hargaTiket = 1500000;
                    break;
                case "Economy":
                    hargaTiket = 1000000;
                    break;
            }
        } else if (ItemKeberangkatan.equalsIgnoreCase("jakarta") && ItemTujuan.equalsIgnoreCase("yogyakarta") || ItemKeberangkatan.equalsIgnoreCase("yogyakarta") && ItemTujuan.equalsIgnoreCase("jakarta")) {

            switch (ItemKelas) {
                case "VVIP":
                    hargaTiket = 1900000;
                    break;
                case "Business":
                    hargaTiket = 1700000;
                    break;
                case "Economy":
                    hargaTiket = 1200000;
                    break;
            }

        } else if (ItemKeberangkatan.equalsIgnoreCase("bandung") && ItemTujuan.equalsIgnoreCase("yogyakarta") || ItemKeberangkatan.equalsIgnoreCase("yogyakarta") && ItemTujuan.equalsIgnoreCase("bandung")) {
            switch (ItemKelas) {
                case "VVIP":
                    hargaTiket = 2100000;
                    break;
                case "Business":
                    hargaTiket = 1900000;
                    break;
                case "Economy":
                    hargaTiket = 1400000;
                    break;
            }
        }

        TextView jumlahText = (TextView) findViewById(R.id.jmlhPenumpang);
        jumlah = Integer.parseInt(String.valueOf(jumlahText.getText()));

        System.out.println(jumlah);
        System.out.println(hargaTiket);
        hargaTotal = hargaTiket * jumlah;
    }

    //selanjutnya kita buat methode fungtion untuk mengirim data ke database
    public void KirimData(final String namaPenumpang, final String kelas, final String date, final Integer hargaTiket, final String keberangkatan, final String tujuan) {
        dbReservation.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                HashMap<String, String> map = new HashMap<String, String>();

                map.put("namaPenumpang", namaPenumpang);
                map.put("kelas", kelas);
                map.put("date", date);
                map.put("hargaTiket", String.valueOf(hargaTiket));
                map.put("keberangkatan", keberangkatan);
                map.put("tujuan", tujuan);

                dbReservation.setValue(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(), "data berhasil dikirim", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }

        });

    }

}